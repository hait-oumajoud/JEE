package tsi.ensg.jee;
import org.hibernate.SessionFactory;
import java.util.Optional;
import java.util.Random;

public class Application {

    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtils.createSessionFactory();
        EmployeeDAO empDAO = new EmployeeDAO(sessionFactory);
        System.out.println("TEST CREATE DATA");
        empDAO.create( "Boby", "Lapointe", 500);
        empDAO.create( "Boris", "Van", 600);
        empDAO.create( "Lisa", "Simpson", 100);
        empDAO.create( "Marge", "Simpson", 1000);
        empDAO.create( "Homer", "Simpson", 450);
        System.out.println("\n On supprime Lisa Simpson de la base de données");
        empDAO.delete(3);
        empDAO.getAll().forEach((EmployeeData) -> System.out.println(EmployeeData.toString()));
        System.out.println("\n On augmente le salaire de Homer Simpson de 100");
        int salary = empDAO.get(5).get().getSalary() +100;
        empDAO.update(5, salary);
        empDAO.getAll().forEach((EmployeeData) -> System.out.println(EmployeeData.toString()));
        System.out.println("\n On affiche tous les employés de la base de données");
        empDAO.getAll().forEach((EmployeeData) -> System.out.println(EmployeeData.toString()));
        System.out.println("\n");
        System.out.println("On ajoute 100 euros à tous les employés qui gagnent moins de 550 euros");
        for (final Employee employee : empDAO.getAll()) {
            //System.out.println(emp.toString());
            if( employee.getSalary() < 550)
            {
                empDAO.update(employee.getId(), employee.getSalary()+100);
            }
        }
        empDAO.getAll().forEach((EmployeeData) -> System.out.println(EmployeeData.toString()));
        System.out.println("\n");
        System.out.println("TEST grenvoie la liste des employés ayant un prénom donné avec une requête HQL");
        empDAO.getAllByFirstName("Boris").forEach((EmployeeData) -> System.out.println(EmployeeData.toString()));
    }


}
