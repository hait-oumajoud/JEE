package colloque.session;
import colloque.metier.Event;
import colloque.metier.Participant;
import colloque.metier.Speaker;
import colloque.services.EventService;
import colloque.services.ParticipantService;

public class Application {
    public static void main(String[] args) {
        ParticipantService participantService = new ParticipantService();
        EventService eventService = new EventService();
        System.out.println("TEST CREATE DATA");
        Event loveInParis = new Event("loveInParis", "movie", "11 am", "2 hours");
        Event hateInParis = new Event("hateInParis", "movie", "11 am", "2 hours");
        Event justParis = new Event("justParis", "movie", "11 am", "2 hours");
        eventService.create(loveInParis);
        eventService.create(hateInParis);
        eventService.create(justParis);
        Participant boby = new Participant("Boby", "Lapointe", "BobyLapointe@gmail.com", loveInParis);
        Participant boris = new Participant("Boris", "Van", "BorisVan@gmail.com", hateInParis);
        Speaker enzoV = new Speaker("Enzo", "Venon", "EnzoVenon@gmail.com", justParis, "0614123215","LeVEnzo" );
        participantService.create(boby);
        participantService.create(boris);
        participantService.create(enzoV);
    }
}
