package tsi.ensg.jee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.List;
import java.util.Optional;

public class EmployeeDAO {
    private final SessionFactory sessionFactory ;
    public EmployeeDAO(SessionFactory sessionFactory){this.sessionFactory = sessionFactory;}
    public long create(String firstName, String lastName, int salary) {
        Employee emp = new Employee(firstName, lastName, salary);
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.persist(emp);
        session.getTransaction().commit();
        session.close();

        return 1;
    }
    public boolean delete(long id) {
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        try {
            session.delete(session.get(Employee.class, id));
            session.getTransaction().commit();
            session.close();
            return true;
        }
        catch (Exception e)
        {
            session.getTransaction().commit();
            session.close();
            return false;
        }    }
    public boolean update(long id, int salary) {
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        try{
            Employee emp = (Employee) session.get(Employee.class, id);
            emp.setSalary(salary);
            session.update(emp);
            session.getTransaction().commit();
            session.close();
            return true;
        }
        catch (Exception e)
        {
            session.getTransaction().commit();
            session.close();
            return false;
        }
    }
    public boolean update(long id, float pourcent) {
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        try{
            Employee emp = (Employee) session.get(Employee.class, id);
            emp.setSalary((int) (emp.getSalary() + (emp.getSalary() * pourcent)));
            session.update(emp);
            session.getTransaction().commit();
            session.close();
            return true;

        }
        catch (Exception e)
        {
            session.getTransaction().commit();
            session.close();
            return false;
        }
    }
    public boolean update(long id, int ConditionInferiorite, int addToSalary) {
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        try{
            Employee emp = (Employee) session.get(Employee.class, id);
            if(emp.getSalary() <ConditionInferiorite)
            {
                emp.setSalary((int) (emp.getSalary() + addToSalary));
                session.update(emp);
            }
            session.getTransaction().commit();
            session.close();
            return true;

        }
        catch (Exception e)
        {
            session.getTransaction().commit();
            session.close();
            return false;
        }
    }
    public Optional<Employee> get(long id) {
        Session session = this.sessionFactory.openSession();
        System.out.println( session.get(Employee.class, id));
        Optional<Employee> employee = Optional.ofNullable(session.get(Employee.class, id));
        session.close();
        return employee;
    }

    public List<Employee> getAll() {
        Session session = this.sessionFactory.openSession();
        List res = session.createQuery("from Employee").list();
        session.close();
        return res;
    }

    public List<Employee>  getAllByFirstName(String firstName)
    {
        Session session = this.sessionFactory.openSession();
        List res = session.createQuery("from Employee where firstName = :firstName").setParameter("firstName", firstName).list();
        session.close();
        return res;
    }
}
