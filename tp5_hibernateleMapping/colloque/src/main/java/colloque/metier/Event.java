package colloque.metier;

import org.hibernate.annotations.GenericGenerator;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="Event")
public class Event {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "id")
    private long id;
    @Column(name = "title")
    private String title;
    @Column(name = "theme")
    private String theme;
    @Column(name = "startDate")
    private String startDate;
    @Column(name = "duration")
    private String duration;
    @Column(name = "NbPartMax")
    private int NbPartMax;
    @Column(name = "organisation")
    private String organisation;
    @Column(name = "description")
    private String observation;
    @Column(name = "typeEvent")
    private String typeEvent;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "event")
    private List<Participant> participants = new ArrayList<>();
    public Event() {}
    public Event(String title, String theme, String startDate, String duration, int nbPartMax, String organisation, String observation, String typeEvent) {
        this.title = title;
        this.theme = theme;
        this.startDate = startDate;
        this.duration = duration;
        this.NbPartMax = nbPartMax;
        this.organisation = organisation;
        this.observation = observation;
        this.typeEvent = typeEvent;
    }
    public Event(String title, String theme, String startDate, String duration) {
        this.title = title;
        this.theme = theme;
        this.startDate = startDate;
        this.duration = duration;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getNbPartMax() {
        return NbPartMax;
    }

    public void setNbPartMax(int nbPartMax) {
        NbPartMax = nbPartMax;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(String typeEvent) {
        this.typeEvent = typeEvent;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", theme='" + theme + '\'' +
                ", startDate=" + startDate +
                ", duration=" + duration +
                ", NbPartMax=" + NbPartMax +
                ", organisation='" + organisation + '\'' +
                ", observation='" + observation + '\'' +
                ", typeEvent='" + typeEvent + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return id == event.id && NbPartMax == event.NbPartMax && title.equals(event.title) && theme.equals(event.theme) && startDate.equals(event.startDate) && duration.equals(event.duration) && organisation.equals(event.organisation) && observation.equals(event.observation) && typeEvent.equals(event.typeEvent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, theme, startDate, duration, NbPartMax, organisation, observation, typeEvent);
    }
}
