package colloque.services;

import colloque.metier.Event;
import colloque.session.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.List;
import java.util.Optional;

public class EventService {
    private SessionFactory sessionFactory ;
    public EventService(){this.sessionFactory = (new HibernateUtils()).sessionFactory;}

    public long create(Event event) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.persist(event);
        session.getTransaction().commit();
        session.close();
        return 1;
    }
    public boolean delete(long id) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        try {
            session.delete(session.get(Event.class, id));
            session.getTransaction().commit();
            session.close();
            return true;
        }
        catch (Exception e)
        {
            session.getTransaction().commit();
            session.close();
            return false;
        }    }
    public boolean update(long id, String title,String theme , String startDate,String duration ) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        try{
            Event event = (Event) session.get(Event.class, id);
            event.setTitle(title);
            event.setTheme(theme);
            event.setStartDate(startDate);
            event.setDuration(duration);
            session.update(event);
            session.getTransaction().commit();
            session.close();
            return true;
        }
        catch (Exception e)
        {
            session.getTransaction().commit();
            session.close();
            return false;
        }
    }
    public boolean updateTitle(long id, String title) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        try{
            Event event = (Event) session.get(Event.class, id);
            event.setTitle(title);
            session.update(event);
            session.getTransaction().commit();
            session.close();
            return true;
        }
        catch (Exception e)
        {
            session.getTransaction().commit();
            session.close();
            return false;
        }
    }

    public boolean updateTheme(long id, String theme) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        try{
            Event event = (Event) session.get(Event.class, id);
            event.setTitle(theme);
            session.update(event);
            session.getTransaction().commit();
            session.close();
            return true;
        }
        catch (Exception e)
        {
            session.getTransaction().commit();
            session.close();
            return false;
        }
    }
    public boolean updateStartDate(long id, String startDate) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        try{
            Event event = (Event) session.get(Event.class, id);
            event.setTitle(startDate);
            session.update(event);
            session.getTransaction().commit();
            session.close();
            return true;
        }
        catch (Exception e)
        {
            session.getTransaction().commit();
            session.close();
            return false;
        }
    }
    public boolean updateDuration(long id, String duration) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        try{
            Event event = (Event) session.get(Event.class, id);
            event.setTitle(duration);
            session.update(event);
            session.getTransaction().commit();
            session.close();
            return true;
        }
        catch (Exception e)
        {
            session.getTransaction().commit();
            session.close();
            return false;
        }
    }
    public Optional<Event> get(long id) {
        Session session = sessionFactory.openSession();
        System.out.println( session.get(Event.class, id));
        Optional<Event> event = Optional.ofNullable(session.get(Event.class, id));
        session.close();
        return event;
    }

    public List<Event> getAll() {
        Session session = sessionFactory.openSession();
        List res = session.createQuery("from event").list();
        session.close();
        return res;
    }

    public List<Event>  getAllByTitle(String title)
    {
        Session session = sessionFactory.openSession();
        List res = session.createQuery("from event where title = :title").setParameter("title", title).list();
        session.close();
        return res;
    }
}
