package tsi.ensg.jee.tp2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class QuestionsService {
    @Autowired
    @Qualifier("java")
    public Questions questions;
    //public QuestionsService(){}
    public void setQuestions(Questions questions){this.questions = questions;}

    public String randomQuestion(){return questions.questions().get(0);}

}
