package tsi.ensg.jee.tp2;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
@Component("java_avance")
@Primary
public class QuestionsJavaAvance implements Questions {
    public List<String> questions;

    public QuestionsJavaAvance(){
        questions= Collections.unmodifiableList(List.of("QV1","QV2"));
    }
    public void setQuestions(List<String> questions) {
        this.questions=List.copyOf(questions);
    }

    @Override
    public List<String> questions() {
        return questions;
    }
}
