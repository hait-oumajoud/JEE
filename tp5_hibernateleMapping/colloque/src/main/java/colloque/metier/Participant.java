package colloque.metier;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity(name="Participant")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Participant {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "id")
    private long id;
    @Column(name = "firstName", nullable=false)
    private String firstName;
    @Column(name = "lastName", nullable=false)
    private String lastName;
    @Column(name = "email", nullable=false)
    private String email;
    @Column(name = "birthDate")
    private Date birthDate;
    @Column(name = "organisation")
    private String organisation;
    @Column(name = "observation")
    private String observation;
    @ManyToOne
    private Event event;
    public Participant(){}

    public Participant(String firstName, String lastName, String email, Event event) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.event=event;
    }

    public Participant(String firstName, String lastName, String email, Date birthDate, String organisation, String observation, Event event) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.birthDate = birthDate;
        this.organisation = organisation;
        this.observation = observation;
        this.event=event;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public String getOrganisation() {
        return organisation;
    }

    public String getObservation() {
        return observation;
    }

    @Override
    public String toString() {
        return "Participant{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", birthDate=" + birthDate +
                ", organisation='" + organisation + '\'' +
                ", observation='" + observation + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Participant that = (Participant) o;
        return id == that.id && firstName.equals(that.firstName) && lastName.equals(that.lastName) && email.equals(that.email) && birthDate.equals(that.birthDate) && organisation.equals(that.organisation) && observation.equals(that.observation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, email, birthDate, organisation, observation);
    }
}
