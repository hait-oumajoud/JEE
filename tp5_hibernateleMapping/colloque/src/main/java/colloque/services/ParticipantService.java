package colloque.services;

import colloque.metier.Participant;
import colloque.session.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Optional;

public class ParticipantService {
    private SessionFactory sessionFactory ;
    public ParticipantService(){this.sessionFactory = (new HibernateUtils()).sessionFactory;}

    public long create(Participant participant) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.persist(participant);
        session.getTransaction().commit();
        session.close();
        return 1;
    }
    public boolean delete(long id) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        try {
            session.delete(session.get(Participant.class, id));
            session.getTransaction().commit();
            session.close();
            return true;
        }
        catch (Exception e)
        {
            session.getTransaction().commit();
            session.close();
            return false;
        }    }
    public boolean update(long id, String firstName, String lastName, String email) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        try{
            Participant participant = (Participant) session.get(Participant.class, id);
            participant.setFirstName(firstName);
            participant.setLastName(lastName);
            participant.setEmail(email);
            session.update(participant);
            session.getTransaction().commit();
            session.close();
            return true;
        }
        catch (Exception e)
        {
            session.getTransaction().commit();
            session.close();
            return false;
        }
    }
    public boolean updateFirstName(long id, String firstName) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        try{
            Participant participant = (Participant) session.get(Participant.class, id);
            participant.setFirstName(firstName);
            session.update(participant);
            session.getTransaction().commit();
            session.close();
            return true;
        }
        catch (Exception e)
        {
            session.getTransaction().commit();
            session.close();
            return false;
        }
    }
    public boolean updateLastName(long id, String lastName) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        try{
            Participant participant = (Participant) session.get(Participant.class, id);
            participant.setLastName(lastName);
            session.update(participant);
            session.getTransaction().commit();
            session.close();
            return true;
        }
        catch (Exception e)
        {
            session.getTransaction().commit();
            session.close();
            return false;
        }
    }
    public boolean updateEmail(long id, String email) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        try{
            Participant participant = (Participant) session.get(Participant.class, id);
            participant.setEmail(email);
            session.update(participant);
            session.getTransaction().commit();
            session.close();
            return true;
        }
        catch (Exception e)
        {
            session.getTransaction().commit();
            session.close();
            return false;
        }
    }
    public Optional<Participant> get(long id) {
        Session session = sessionFactory.openSession();
        System.out.println( session.get(Participant.class, id));
        Optional<Participant> participant = Optional.ofNullable(session.get(Participant.class, id));
        session.close();
        return participant;
    }

    public List<Participant> getAll() {
        Session session = sessionFactory.openSession();
        List res = session.createQuery("from participant").list();
        session.close();
        return res;
    }

    public List<Participant>  getAllByFirstName(String firstName)
    {
        Session session = sessionFactory.openSession();
        List res = session.createQuery("from participant where firstName = :firstName").setParameter("firstName", firstName).list();
        session.close();
        return res;
    }
}
