package tsi.ensg.jee.tp2;

import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
@Component("java")
public class QuestionsJava implements Questions {
    private List<String> questions;

    public QuestionsJava(){
        questions= Collections.unmodifiableList(List.of("Q1","Q2"));

    }
    public void setQuestions(List<String> questions) {this.questions=List.copyOf(questions);}
    @Override
    public List<String> questions() {return questions;}
}
