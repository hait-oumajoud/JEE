package colloque.metier;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity(name="Speaker")
public class Speaker extends Participant{
    private String phoneNumber;
    private String titleSpeaker;

    public Speaker() {}

    public Speaker(String firstName, String lastName, String email, Event event, String phoneNumber, String titleSpeaker) {
        super(firstName, lastName, email, event);
        this.phoneNumber = phoneNumber;
        this.titleSpeaker = titleSpeaker;
    }

    public Speaker(String firstName, String lastName, String email, Date birthDate, String organisation, String observation, Event event, String phoneNumber, String titleSpeaker) {
        super(firstName, lastName, email, birthDate, organisation, observation, event);
        this.phoneNumber = phoneNumber;
        this.titleSpeaker = titleSpeaker;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getTitleSpeaker() {
        return titleSpeaker;
    }

    public void setTitleSpeaker(String titleSpeaker) {
        this.titleSpeaker = titleSpeaker;
    }
}
